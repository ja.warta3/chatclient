import React from 'react'
import ReactDOM from 'react-dom'

const socket = new WebSocket("ws://rescuetheweather.com:10000/ws");

class MyChat extends React.Component {

  constructor () {
    super();
	this.state = {
	  chatBox: "",   // value for the big textarea that holds all the messages back and forth
	  username: "",  // username of the person sending the message
	  sendTo: "",    // username of the person that the message should be sent to
      messageBody: ""// current message being sent
	};
  }

// making a change here

  render() {
    return (
	  <div>
	  <textarea id="chatbox" rows="25" cols="75" value={this.state.chatBox}></textarea><br/>	
	  <label for="inputTwo">Username</label>
	  <input name="username" onChange={evt => this.updateInputState(evt)} /><br/>
	  <label for="inputThree">Send To:</label>
	  <input name="sendTo" onChange={evt => this.updateInputState(evt)} /><br/>
	  <label for="input">Message</label>
	  <input name="messageBody" onChange={evt => this.updateInputState(evt)} /><br/>
      <button id="buttonOne" onClick={() => this.sendMessage()}>Send</button>
	  </div>
	);
  }
  
  // when one of the input fields is altered (somebody types something in), this will fire...
  updateInputState (evt) {
    this.setState({
	  [evt.target.name]: evt.target.value
	});
  }
  
  // This fires immediately after components are rendered
  componentDidMount() {
    socket.onopen = () => {
      console.log("Successfully Connected");
      //socket.send("Hi From the Client!")
    };
        
    socket.onclose = event => {
      console.log("Socket Closed Connection: ", event);
      socket.send("Client Closed!")
    };

    socket.onerror = error => {
      console.log("Socket Error: ", error);
    };
	
	socket.onmessage = event => {
      //alert(e.data);	
	  // grab all the previous messages
	  var curChatBoxState = this.state.chatBox;
	  var sendTo = this.state.sendTo;
	  var messageBody = event.data;
	  // combine the new message we're sending with the old messages that have already been sent...
	  var newChatBoxState = curChatBoxState + sendTo + ": " + messageBody + "\n";
	  // ... and update the chat box with all the messages
	  this.setState({
	    chatBox: newChatBoxState
	  });
	}
	
  }
  
  sendMessage = () => {
    //var message = JSON.stringify(this.state);
    var message = JSON.stringify({
		username: this.state.username,
		sendto: this.state.sendTo,
		body: this.state.messageBody
	});
	socket.send(message);
	
	var curChatBoxState = this.state.chatBox;
	var username = this.state.username;
	var messageBody = this.state.messageBody;
	var newChatBoxState = curChatBoxState + username + ": " + messageBody + "\n";
	this.setState({
	  chatBox: newChatBoxState
	});
	
  } 
  
}

ReactDOM.render(<MyChat />, document.getElementById('root'));
